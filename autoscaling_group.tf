resource "aws_launch_template" "launch-template-projetc-ramy" {
  name_prefix = "launch-template-projetc-ramy"
  
  image_id = "ami-065793e81b1869261"
  instance_type = "t3.micro"
  user_data = base64encode(data.template_file.user_data_04.rendered)

  network_interfaces {
    associate_public_ip_address = true
    security_groups = [aws_security_group.asg-sg-projetc-ramy.id]
  }
  tag_specifications {
 resource_type = "instance"
 tags = {
 Name = "launch-template-projetc-ramy"
 }
# health_check_type = "ELB"
# lifecycle {
#  ignore_changes = [load_balancers, target_group_arns]
# #  owner = var.tag-owner
# #  ephemere = var.tag-ephemere
# #  entity = var.tag-entity
# }
}
}

resource "aws_autoscaling_group" "asg-projetc-ramy" {
  name = "asg-projetc-ramy"
  launch_template {
    id = aws_launch_template.launch-template-projetc-ramy.id
    version = "$Latest"
  }
  min_size = 1
  max_size = 10
  vpc_zone_identifier = [module.module_reseau.private_subnet_1_id, module.module_reseau.private_subnet_2_id]
  health_check_type = "ELB"

lifecycle {
 ignore_changes = [load_balancers, target_group_arns]
}


}