module "module_reseau" {
  source = "./modules/module_reseau"

  cidr_vpc = "150.0.0.0/16"
  cidr_public_subnet_1  = "150.0.1.0/24"
  cidr_public_subnet_2  = "150.0.2.0/24"
  cidr_private_subnet_1 = "150.0.3.0/24"
  cidr_private_subnet_2 = "150.0.4.0/24"
  cidr_private_subnet_3 = "150.0.5.0/24"
  cidr_private_subnet_4 = "150.0.6.0/24"
  tag-entity = "numfactory"
  tag-ephemere = "non"
  tag-owner = "rben-ikhelef@thenuumfactory.fr"
}