terraform {
  backend "s3" {
    bucket = "terraform-backend-projetc-ramy"
    key    = "terraform-04.tfstate"
    region = "eu-west-1"
  }
}