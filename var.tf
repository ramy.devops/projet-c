variable "cluster-name" {
  type    = string
  default = "eks-cluster-projetc-ramy"
}

variable "eksworkernodes-iam-role" {
  type = string
}

variable "eks-iam-role" {
  type = string
}
