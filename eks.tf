# resource "aws_iam_role" "eks-cluster-projetc-ramy" {

#     name ="eks-cluster-projetc-ramy"
#     assume_role_policy = <<POLICY
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Effect": "Allow",
#       "Principal": {
#         "Service": "eks.amazonaws.com"
#       },
#       "Action": "sts:AssumeRole"
#     }
#   ]
# }
# POLICY
# }

# resource "aws_iam_role_policy_attachment" "cluster-policy" {
#   policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
#   role       = "eks-iam-role"
# }

# resource "aws_iam_role_policy_attachment" "service-policy" {
#   policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
#   role       = "eks-iam-role"
# }


resource "aws_eks_cluster" "cluster-eks-projetc-ramy" {
  name            = "cluster-eks-projetc-ramy"
  role_arn        = var.eks-iam-role

  vpc_config {
    endpoint_private_access = false #Je n'utilise pas de bastion
    endpoint_public_access = true
    security_group_ids = [aws_security_group.cluster-eks-sg-projetc-ramy.id]
    subnet_ids         = [module.module_reseau.public_subnet_1_id,
                          module.module_reseau.public_subnet_2_id,
                          module.module_reseau.private_subnet_1_id,
                          module.module_reseau.private_subnet_2_id]
  }

  # depends_on = [
  #   "aws_iam_role_policy_attachment.cluster-policy",
  #   "aws_iam_role_policy_attachment.service-policy",
  # ]
}