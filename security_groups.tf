################################################################## Loadbalancer Security group #########################################################
resource "aws_security_group" "lb-sg-projetc-ramy" {
  name_prefix = "lb-sg-projetc-ramy"
  description = "Security group for load balancer"
  vpc_id = module.module_reseau.vpc_id.id
  
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}
################################################################## EC2 Security group #########################################################
# Security group for EC2 instances in the Auto Scaling Group
resource "aws_security_group" "asg-sg-projetc-ramy" {
  name_prefix = "asg-sg-projetc-ramy"
  description = "Security group for EC2 instances in the Auto Scaling Group"
  vpc_id = module.module_reseau.vpc_id.id

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    security_groups = [aws_security_group.lb-sg-projetc-ramy.id]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}
################################################################## Memcache Security group #########################################################
# Security group for the memcached cluster
resource "aws_security_group" "memcached-sg-projetc-ramy" {
  name_prefix = "memcached-sg-projetc-ramy"
  description = "Security group for memcached cluster"
  vpc_id = module.module_reseau.vpc_id.id

  ingress {
    from_port = 11211
    to_port = 11211
    protocol = "tcp"
    security_groups = [aws_security_group.asg-sg-projetc-ramy.id]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
################################################################## Cluster Eks Security group #########################################################
resource "aws_security_group" "cluster-eks-sg-projetc-ramy" {
  name        = "cluster-eks-sg-projetc-ramy"
  description = "Cluster communication with worker nodes"
  vpc_id      = module.module_reseau.vpc_id.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "cluster-eks-projetc-ramy"
  }
}
# OPTIONAL: Allow inbound traffic from your local workstation external IP
#           to the Kubernetes. You will need to replace A.B.C.D below with
#           your real IP. Services like icanhazip.com can help you find this.
resource "aws_security_group_rule" "cluster-ingress-workstation-https" {
  cidr_blocks       = ["192.168.1.112/32"]
  description       = "Allow workstation to communicate with the cluster API Server"
  from_port         = 443
  protocol          = "tcp"
  security_group_id = "${aws_security_group.cluster-eks-sg-projetc-ramy.id}"
  to_port           = 443
  type              = "ingress"
}

################################################################## Worker Node Security group #########################################################

resource "aws_security_group" "node-eks-sg-projetc-ramy" {
  name        = "node-eks-sg-projetc-ramy"
  description = "Security group for all nodes in the cluster"
  vpc_id      = module.module_reseau.vpc_id.id
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "node-eks-sg-projetc-ramy"
    "kubernetes.io/cluster/${var.cluster-name}" = "owned"
  }
    
}

resource "aws_security_group_rule" "node-ingress-self" {
  description              = "Allow node to communicate with each other"
  from_port                = 0
  protocol                 = "-1"
  security_group_id        = "${aws_security_group.node-eks-sg-projetc-ramy.id}"
  source_security_group_id = "${aws_security_group.node-eks-sg-projetc-ramy.id}"
  to_port                  = 65535
  type                     = "ingress"
}

resource "aws_security_group_rule" "node-ingress-cluster-https" {
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  from_port                = 443
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.node-eks-sg-projetc-ramy.id}"
  source_security_group_id = "${aws_security_group.node-eks-sg-projetc-ramy.id}"
  to_port                  = 443
  type                     = "ingress"
}

resource "aws_security_group_rule" "node-ingress-cluster-others" {
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  from_port                = 1025
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.node-eks-sg-projetc-ramy.id}"
  source_security_group_id = "${aws_security_group.cluster-eks-sg-projetc-ramy.id}"
  to_port                  = 65535
  type                     = "ingress"
}

resource "aws_security_group_rule" "cluster-ingress-node-https" {
  description              = "Allow pods to communicate with the cluster API Server"
  from_port                = 443
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.cluster-eks-sg-projetc-ramy.id}"
  source_security_group_id = "${aws_security_group.node-eks-sg-projetc-ramy.id}"
  to_port                  = 443
  type                     = "ingress"
}