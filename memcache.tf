resource "aws_elasticache_subnet_group" "dev-memcached-sg-projetc-ramy" {
  name       = "dev-memcached-sg-projetc-ramy"
  subnet_ids = [module.module_reseau.private_subnet_3_id, module.module_reseau.private_subnet_4_id]

}


resource "aws_elasticache_cluster" "dev-memcached-projetc-ramy" {
  cluster_id           = "dev-memcached-projetc-ramy"
  engine               = "memcached"
  node_type            = "cache.t3.micro"
  security_group_ids   = [aws_security_group.memcached-sg-projetc-ramy.id]
  subnet_group_name    = aws_elasticache_subnet_group.dev-memcached-sg-projetc-ramy.name
  num_cache_nodes      = 2
  port                 = 11211


}