resource "aws_lb" "lb-projetc-ramy" {
  name               = "lb-projetc-ramy"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.lb-sg-projetc-ramy.id]
  subnets            = [module.module_reseau.public_subnet_1_id, module.module_reseau.public_subnet_2_id]

}

resource "aws_lb_target_group" "tg-projetc-ramy" {
  name     = "nuumfactory-biglab-dev-tg-04"
  port     = 80
  protocol = "HTTP"
  vpc_id = module.module_reseau.vpc_id.id
  
}

resource "aws_lb_listener" "listener" {
  load_balancer_arn = aws_lb.lb-projetc-ramy.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tg-projetc-ramy.arn
  }

}

resource "aws_autoscaling_attachment" "asg-lb-attachment-projetc-ramy" {
  autoscaling_group_name = aws_autoscaling_group.asg-projetc-ramy.id
  alb_target_group_arn    = aws_lb_target_group.tg-projetc-ramy.arn


}