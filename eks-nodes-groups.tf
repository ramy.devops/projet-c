# resource "aws_iam_role" "node-eks-projetc-ramy" {
#   name = "node-eks-projetc-ramy"

#   assume_role_policy = <<POLICY
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Effect": "Allow",
#       "Principal": {
#         "Service": "ec2.amazonaws.com"
#       },
#       "Action": "sts:AssumeRole"
#     }
#   ]
# }
# POLICY
# }

# resource "aws_iam_role_policy_attachment" "worker-node-policy" {
#   policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
#   role       = "eksworkernodes-iam-role"
# }

# resource "aws_iam_role_policy_attachment" "cni-policy" {
#   policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
#   role       = "eksworkernodes-iam-role"
# }

# resource "aws_iam_role_policy_attachment" "ec2-container-registry-readOnly" {
#   policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
#   role       = "eksworkernodes-iam-role"
# }

# resource "aws_iam_instance_profile" "iam-profile-node-eks-projetc-ramy" {
#   name = "iam-profile-node-eks-projetc-ramy"
#   role = "eksworkernodes-iam-role"
# }

resource "aws_eks_node_group" "nodes-eks-projetc-ramy" {
    cluster_name = var.cluster-name

    node_group_name = "nodes-eks-projetc-ramy"
    node_role_arn = var.eksworkernodes-iam-role

    subnet_ids = [ module.module_reseau.private_subnet_1_id,
                    module.module_reseau.private_subnet_2_id ]
    
    scaling_config {
      desired_size = 1
      max_size = 1
      min_size = 1
    }
    ami_type = "AL2_x86_64"
    capacity_type = "ON_DEMAND"
    disk_size = 20
    force_update_version = false
    instance_types = ["t3.small"]

    labels = {
        role = "node-general"

    }
    version = "1.18"

    # depends_on = [
    #   aws_iam_role_policy_attachment.worker-node-policy,
    #   aws_iam_role_policy_attachment.cni-policy,
    #   aws_iam_role_policy_attachment.ec2-container-registry-readOnly,
    # ]

}