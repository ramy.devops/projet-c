data "template_file" "user_data_04" {
 template = file("user_data.tftpl")
 vars = {
 memcached_cluster_address = aws_elasticache_cluster.dev-memcached-projetc-ramy.cluster_address
 memcached_cluster_port = aws_elasticache_cluster.dev-memcached-projetc-ramy.port
 }
}
