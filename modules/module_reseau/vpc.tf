resource "aws_vpc" "vpc-projetc-ramy" {
  cidr_block = var.cidr_vpc

  tags = {
    Name = "vpc-projetC-Ramy"
    owner = var.tag-owner
    ephemere = var.tag-ephemere
    entity = var.tag-entity

  }
}