#Public subnet 1
resource "aws_subnet" "public_subnet_1" {
  vpc_id            = aws_vpc.vpc-projetc-ramy.id
  availability_zone = "eu-west-1a"
  cidr_block        = var.cidr_public_subnet_1
  map_public_ip_on_launch = true

  tags = {
    Name = "public-subnet-projetc-ramy-1"
    owner = var.tag-owner
    ephemere = var.tag-ephemere
    entity = var.tag-entity
    "kubernetes.io/cluster/eks" = "shared"
    "kubernetes.io/role/elb" = 1
  }
}


#Public subnet 2
resource "aws_subnet" "public_subnet_2" {
  vpc_id            = aws_vpc.vpc-projetc-ramy.id
  availability_zone = "eu-west-1b"
  cidr_block        = var.cidr_public_subnet_2
  map_public_ip_on_launch = true

  tags = {
    Name = "public-subnet-projetc-ramy-2"
    owner = var.tag-owner
    ephemere = var.tag-ephemere
    entity = var.tag-entity
    "kubernetes.io/cluster/eks" = "shared"
    "kubernetes.io/role/elb" = 1
  }
}

#Private subnet 1
resource "aws_subnet" "private_subnet_1" {
  vpc_id            = aws_vpc.vpc-projetc-ramy.id
  availability_zone = "eu-west-1a"
  cidr_block        = var.cidr_private_subnet_1

  tags = {
    Name = "private-subnet-projetc-ramy-1"
    owner = var.tag-owner
    ephemere = var.tag-ephemere
    entity = var.tag-entity
    "kubernetes.io/cluster/eks" = "shared"
    "kubernetes.io/role/elb" = 1
  }
}

#Private subnet 2
resource "aws_subnet" "private_subnet_2" {
  vpc_id            = aws_vpc.vpc-projetc-ramy.id
  availability_zone = "eu-west-1b"
  cidr_block        = var.cidr_private_subnet_2

  tags = {
    Name = "private-subnet-projetc-ramy-2"
    owner = var.tag-owner
    ephemere = var.tag-ephemere
    entity = var.tag-entity
    "kubernetes.io/cluster/eks" = "shared"
    "kubernetes.io/role/elb" = 1
  }
}

#Private subnet 3
resource "aws_subnet" "private_subnet_3" {
  vpc_id            = aws_vpc.vpc-projetc-ramy.id
  availability_zone = "eu-west-1a"
  cidr_block        = var.cidr_private_subnet_3

  tags = {
    Name = "private-subnet-projetc-ramy-3"
    owner = var.tag-owner
    ephemere = var.tag-ephemere
    entity = var.tag-entity
  }
}

#Private subnet 4
resource "aws_subnet" "private_subnet_4" {
  vpc_id            = aws_vpc.vpc-projetc-ramy.id
  availability_zone = "eu-west-1b"
  cidr_block        = var.cidr_private_subnet_4

  tags = {
    Name = "private-subnet-projetc-ramy-4"
    owner = var.tag-owner
    ephemere = var.tag-ephemere
    entity = var.tag-entity
  }
}
