resource "aws_internet_gateway" "ig-projetc-ramy" {
    vpc_id = aws_vpc.vpc-projetc-ramy.id
  tags = {
    Name = "ig-projetc-ramy"
    owner = var.tag-owner
    ephemere = var.tag-ephemere
    entity = var.tag-entity

  }
}