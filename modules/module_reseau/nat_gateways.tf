
resource "aws_eip" "eip-projetc-ramy-1" {
  vpc = true
}

resource "aws_nat_gateway" "natgw-projetc-ramy-1" {
  allocation_id = aws_eip.eip-projetc-ramy-1.id
  subnet_id     = aws_subnet.public_subnet_1.id

  tags = {
    Name = "natgw-projetc-ramy-1"
    owner = var.tag-owner
    ephemere = var.tag-ephemere
    entity = var.tag-entity

  }
  depends_on = [aws_internet_gateway.ig-projetc-ramy]
}

resource "aws_eip" "eip-projetc-ramy-2" {
  vpc = true
}

resource "aws_nat_gateway" "natgw-projetc-ramy-2" {
  allocation_id = aws_eip.eip-projetc-ramy-2.id
  subnet_id     = aws_subnet.public_subnet_2.id

  tags = {
    Name = "natgw-projetc-ramy-2"
    owner = var.tag-owner
    ephemere = var.tag-ephemere
    entity = var.tag-entity

  }
  depends_on = [aws_internet_gateway.ig-projetc-ramy]
}