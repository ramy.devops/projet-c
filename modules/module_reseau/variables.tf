variable "cidr_vpc" {
  description = "L'adresse de sous-réseau du VPC "
  type        = string
}

variable "cidr_public_subnet_1" {
  description = "L'adresse de sous-réseau du sous-réseau public 1 "
  type        = string
}

variable "cidr_public_subnet_2" {
  description = "L'adresse de sous-réseau du sous-réseau public2 "
  type        = string
}

variable "cidr_private_subnet_1" {
  description = "L'adresse de sous-réseau du sous-réseau privée 1 "
  type        = string
}

variable "cidr_private_subnet_2" {
  description = "L'adresse de sous-réseau du sous-réseau privée 2 "
  type        = string
}

variable "cidr_private_subnet_3" {
  description = "L'adresse de sous-réseau du sous-réseau privé 3 "
  type        = string
}

variable "cidr_private_subnet_4" {
  description = "L'adresse de sous-réseau du sous-réseau privé 4 "
  type        = string
}

variable "tag-owner" {
  description = "owner rben-ikhelef@thenuumfactory.fr"
  type = string
}

variable "tag-entity" {
  description = "entity numfactory"
  type = string
}

variable "tag-ephemere" {
  description = "oui ou non"
  type = string
}
