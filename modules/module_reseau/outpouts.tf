output "vpc_id" {
  value = aws_vpc.vpc-projetc-ramy
}

output "vpc_name" {
  value = aws_vpc.vpc-projetc-ramy
}

output "public_subnet_1_id" {
  value = aws_subnet.public_subnet_1.id
}

output "public_subnet_1_name" {
  value = aws_subnet.public_subnet_1.tags["Name"]
}

output "public_subnet_2_id" {
  value = aws_subnet.public_subnet_2.id
}

output "public_subnet_2_name" {
  value = aws_subnet.public_subnet_2.tags["Name"]
}

output "private_subnet_1_id" {
  value = aws_subnet.private_subnet_2.id
}

output "private_subnet_1_name" {
  value = aws_subnet.private_subnet_2.tags["Name"]
}

output "private_subnet_2_id" {
  value = aws_subnet.private_subnet_2.id
}

output "private_subnet_2_name" {
  value = aws_subnet.private_subnet_2.tags["Name"]
}

output "private_subnet_3_id" {
  value = aws_subnet.private_subnet_3.id
}

output "private_subnet_3_name" {
  value = aws_subnet.private_subnet_3.tags["Name"]
}

output "private_subnet_4_id" {
  value = aws_subnet.private_subnet_4.id
}

output "private_subnet_4_name" {
  value = aws_subnet.private_subnet_4.tags["Name"]
}