# Création de la table de routage pour le sous-réseau public 1
resource "aws_route_table" "public_rtb_1" {
  vpc_id = aws_vpc.vpc-projetc-ramy.id

  tags = {
    Name = "public-rtb-projetc-ramy-1"
    owner = var.tag-owner
    ephemere = var.tag-ephemere
    entity = var.tag-entity

  }
}

# Création d'une règle de forwarding pour le sous-réseau public 1 vers l'Internet Gateway
resource "aws_route" "public_rtb_1_igw" {
  route_table_id = aws_route_table.public_rtb_1.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.ig-projetc-ramy.id
}

# Création de la table de routage pour le sous-réseau public 2
resource "aws_route_table" "public_rtb_2" {
  vpc_id = aws_vpc.vpc-projetc-ramy.id

  tags = {
    Name = "public-rtb-projetc-ramy-2"
    owner = var.tag-owner
    ephemere = var.tag-ephemere
    entity = var.tag-entity

  }
}

# Création d'une règle de forwarding pour le sous-réseau public 2 vers l'Internet Gateway
resource "aws_route" "public_rtb_2_igw" {
  route_table_id = aws_route_table.public_rtb_2.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.ig-projetc-ramy.id
}

# Création de la table de routage pour le sous-réseau privé 1
resource "aws_route_table" "private_rtb_1" {
  vpc_id = aws_vpc.vpc-projetc-ramy.id

  tags = {
    Name = "private-rtb-projetc-ramy-1"
    owner = var.tag-owner
    ephemere = var.tag-ephemere
    entity = var.tag-entity

  }
}



# Création de la table de routage pour le sous-réseau privé 2
resource "aws_route_table" "private_rtb_2" {
  vpc_id = aws_vpc.vpc-projetc-ramy.id

  tags = {
    Name = "private-rtb-projetc-ramy-2"
    owner = var.tag-owner
    ephemere = var.tag-ephemere
    entity = var.tag-entity

  }
}

resource "aws_route_table" "private_rtb_3" {
  vpc_id = aws_vpc.vpc-projetc-ramy.id

  tags = {
    Name = "private-rtb-projetc-ramy-3"
    owner = var.tag-owner
    ephemere = var.tag-ephemere
    entity = var.tag-entity

  }
}

resource "aws_route_table" "private_rtb_4" {
  vpc_id = aws_vpc.vpc-projetc-ramy.id

  tags = {
    Name = "private-rtb-projetc-ramy-4"
    owner = var.tag-owner
    ephemere = var.tag-ephemere
    entity = var.tag-entity
  }
}

# Création d'une règle de forwarding pour le sous-réseau privé 1 vers la NAT Gateway du sous-réseau public 1
resource "aws_route" "private_rtb_1_natgw" {
  route_table_id = aws_route_table.private_rtb_1.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id = aws_nat_gateway.natgw-projetc-ramy-1.id
}

# Création d'une règle de forwarding pour le sous-réseau privé 2 vers la NAT Gateway du sous-réseau public 2
resource "aws_route" "private_rtb_2_natgw" {
  route_table_id = aws_route_table.private_rtb_2.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id = aws_nat_gateway.natgw-projetc-ramy-2.id
}

# resource "aws_route" "private_rtb_3_natgw" {
#   route_table_id = aws_route_table.private_rtb_3.id
#   destination_cidr_block = "0.0.0.0/0"
#   nat_gateway_id = aws_nat_gateway.nuumfactory-biglab-natgw-3-04.id
# }


# resource "aws_route" "private_rtb_4_natgw" {
#   route_table_id = aws_route_table.private_rtb_4.id
#   destination_cidr_block = "0.0.0.0/0"
#   nat_gateway_id = aws_nat_gateway.nuumfactory-biglab-natgw-4-04.id
# }



resource "aws_route_table_association" "public_1" {
  subnet_id      = aws_subnet.public_subnet_1.id
  route_table_id = aws_route_table.public_rtb_2.id
}


resource "aws_route_table_association" "public_2" {
  subnet_id      = aws_subnet.public_subnet_2.id
  route_table_id = aws_route_table.public_rtb_2.id
}

resource "aws_route_table_association" "private_1" {
  subnet_id      = aws_subnet.private_subnet_1.id
  route_table_id = aws_route_table.private_rtb_1.id
}


resource "aws_route_table_association" "private_subnet_2" {
  subnet_id      = aws_subnet.private_subnet_2.id
  route_table_id = aws_route_table.private_rtb_2.id
}

resource "aws_route_table_association" "private_subnet_3" {
  subnet_id      = aws_subnet.private_subnet_3.id
  route_table_id = aws_route_table.private_rtb_3.id
}

resource "aws_route_table_association" "private_subnet_4" {
  subnet_id      = aws_subnet.private_subnet_4.id
  route_table_id = aws_route_table.private_rtb_4.id
}